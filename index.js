// Default Setup of Express
//  "require" to load the module/package

const express = require("express");

// Creation of an app/project
// In layman's terms, app is our server
const app = express();
const port = 3000;

// Setup for allowing the server to handle data from request
// Middlewares(para gumana ng maayos si express)

app.use(express.json());//allowing system to handle API(middleware:ByDefault)

//  allows your app to read data from forms(middleware:ByDefault): para mabasa ni express yung mga na import na data or info
app.use(express.urlencoded({extended:true}))

// [Section] - Routes

// GET Method-lower in express.js uppercase pag Node.js
app.get("/", (req,res) =>{
	res.send("Hello World!")
});

// POST METHOD
// automic na body if mag nanalagy ng data
app.post("/hello",(req,res) => {
	res.send(`Hello There ${req.body.firstName} ${req.body.lastName}!`)
});


// MOCK DATABASE
// An array that will store user object when the "/signup" route us accessed
// This will serve as our mock database

let users = [];

// POST METHOD -SIGNUP
app.post("/signup", (req,res) => {
	console.log(req.body);

	// if the username and password is not empty the user info will be pushed /stored to our mock database
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
	}else{
		res.send(`Please input BOTH username and password`)
	}
})

// PUT/UPDATE METHOD
app.put("/change-password", (req,res) => {
	// Creates a variable to store the message to be sent back to the client.
	let message;

	// Create a for loop the will loop through the elements of "users" array

	for(let i = 0; i < users.length; i++){
		// If the username provided in the client/postman and the username of the current objects is the same
		if(req.body.username == user[i].username){
			// Changes the password of the user found the loop
			users[i].password = req.body.password;

			message = `user ${req.body.username}'s has been updated.`;

			break;//use break if user has beem found to stop the loop
		}else{
			// if no user found
			message = "User does not exist";
		}
	}

	res.send(message);//depend sa pumasok sa if & else 

});

// CODE ALONG ACTIVITY

// Home page

app.get("/home", (req,res) =>{
	res.send("Welcom to Homepage")
});

// REGISTERED USERS
app.get("/users", (req,res) =>{
	res.send(users)
});

// DELETE USER
app.delete("/delete-user", (req,res) => {
	let message;
	if(users.length !== 0){
		for(let i = 0 ; i < users.length ; i++){
			if(req.body.username === users[i].username){
				users.splice(users.indexOf[i], 1 ,);
				message = `User ${req.body.username} has been deleted`;
				break;
			}else{
				message = "User does not exist";
			}
		}
	}else{
		essage = "users is empty";
	}
	res.send(message);
})








app.listen(port, ()=> console.log(`Server is running at port ${port}.`));




